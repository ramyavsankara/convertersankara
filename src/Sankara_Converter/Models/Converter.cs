﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sankara_Converter.Models
{
    public class Converter

    {
        [RegularExpression(@"\d{5}$", ErrorMessage = "Please Enter a valid US Zip code")]
        public int ZIP_code { get; set; }
        [Range(-12, 200)]
        public int Temp_F { get; set; }
    }
}
