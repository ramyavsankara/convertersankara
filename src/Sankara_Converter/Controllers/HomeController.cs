﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Sankara_Converter.Models;

namespace Sankara_Converter.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Sankara";
            ViewData["Result"] = "";

            Converter converter = new Converter();


            return View(converter);
        }



        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Sankara";
                ViewData["Result"] = "Temperature in C = " + (int)((converter.Temp_F - 32) * 5.0 / 9.0);
            }
            return View("Index", converter);
        }
    }
    }     
